use iref::Iri;
use json_ld::{
    syntax::Parse, Compact, ExpandedDocument, JsonLdProcessor, Print, Process, RemoteDocument,
    RemoteDocumentReference, ReqwestLoader, TryFromJson,
};
use json_ld_syntax::TryFromJson as _;
use locspan::{Location, Span};
use rdf_types::{generator::Blank, IriVocabularyMut};
use reqwest::Client;
use static_iref::iri;
use std::time::Instant;

const SIMPLE_CONTEXT: &str = r#"[
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1"
]"#;

const MASTODON_CONTEXT: &str = r#"[
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
        "manuallyApprovesFollowers":"as:manuallyApprovesFollowers",
        "toot":"http://joinmastodon.org/ns#",
        "featured":{
            "@id":"toot:featured",
            "@type":"@id"
        },
        "featuredTags":{
            "@id":"toot:featuredTags",
            "@type":"@id"
        },
        "alsoKnownAs":{
            "@id":"as:alsoKnownAs",
            "@type":"@id"
        },
        "movedTo":{
            "@id":"as:movedTo",
            "@type":"@id"
        },
        "schema":"http://schema.org#",
        "PropertyValue":"schema:PropertyValue",
        "value":"schema:value",
        "discoverable":"toot:discoverable",
        "Device":"toot:Device",
        "Ed25519Signature":"toot:Ed25519Signature",
        "Ed25519Key":"toot:Ed25519Key",
        "Curve25519Key":"toot:Curve25519Key",
        "EncryptedMessage":"toot:EncryptedMessage",
        "publicKeyBase64":"toot:publicKeyBase64",
        "deviceId":"toot:deviceId",
        "claim":{
            "@type":"@id",
            "@id":"toot:claim"
        },
        "fingerprintKey":{
            "@type":"@id",
            "@id":"toot:fingerprintKey"
        },
        "identityKey":{
            "@type":"@id",
            "@id":"toot:identityKey"
        },
        "devices":{
            "@type":"@id",
            "@id":"toot:devices"
        },
        "messageFranking":"toot:messageFranking",
        "messageType":"toot:messageType",
        "cipherText":"toot:cipherText",
        "suspended":"toot:suspended",
        "Hashtag": "as:Hashtag",
        "focalPoint":{
            "@container":"@list",
            "@id":"toot:focalPoint"
        }
    }
]"#;

type AnyError = Box<dyn std::error::Error + Send + Sync>;

#[tokio::main]
async fn main() -> Result<(), AnyError> {
    let client = Client::builder().user_agent("json-ld-playground").build()?;

    let iris = [
        iri!("https://masto.asonix.dog/users/asonix/outbox?page=true"),
        iri!("https://relay.asonix.dog/actor"),
        iri!("https://masto.asonix.dog/actor"),
        iri!("https://masto.asonix.dog/users/asonix"),
        iri!("https://masto.asonix.dog/users/kumu"),
        iri!("https://yiff.life/users/6my"),
        iri!("https://meow.social/users/6my"),
    ];

    for iri in iris {
        let document = client
            .get(iri.as_str())
            .header("accept", "application/activity+json")
            .send()
            .await?
            .text()
            .await?;

        normalize_document(iri, &document).await?;
    }

    Ok(())
}

async fn normalize_document(iri: Iri<'static>, document: &str) -> Result<(), AnyError> {
    let mut vocabulary: rdf_types::IndexVocabulary = rdf_types::IndexVocabulary::new();

    let iri_index = vocabulary.insert(iri);

    let input = RemoteDocument::new(
        Some(iri_index.clone()),
        Some("application/activity+json".parse()?),
        json_ld_syntax::Value::parse_str(document, |span| Location::new(iri_index.clone(), span))?,
    );

    let mut loader = ReqwestLoader::default();

    let mut expanded = input.expand_with(&mut vocabulary, &mut loader).await?;

    let mut pre_gen = Blank::new().with_metadata(Location::new(iri_index.clone(), Span::default()));

    expanded.identify_all_with(&mut vocabulary, &mut pre_gen);

    let start = Instant::now();
    let output_document = json_ld_normalization::normalize::<_, _, _, sha2::Sha256>(
        &mut vocabulary,
        iri_index,
        expanded.0,
        true,
    )?;
    println!("normalization took {}ms", start.elapsed().as_millis());

    let start = Instant::now();
    let serialized = json_ld_serialization::rdf_to_json_ld(
        output_document.into_quads(),
        Location::new(iri_index.clone(), Span::default()),
        true,
        None,
        true,
        false,
        &vocabulary,
    )?;

    println!("serialization took {}ms", start.elapsed().as_millis());

    let expanded = ExpandedDocument::try_from_json_in(&mut vocabulary, serialized)
        .expect("Invalid expanded json");

    for context in [MASTODON_CONTEXT, SIMPLE_CONTEXT] {
        let context = RemoteDocumentReference::Loaded(RemoteDocument::new(
            Some(iri_index.clone()),
            Some("application/ld+json".parse()?),
            json_ld_syntax::context::Value::try_from_json(json_ld_syntax::Value::parse_str(
                context,
                |span| Location::new(iri_index.clone(), span),
            )?)?,
        ))
        .load_context_with(&mut vocabulary, &mut loader)
        .await
        .expect("Context is loaded")
        .into_document();

        let processed = context.process(&mut vocabulary, &mut loader, None).await?;

        let compacted = expanded
            .compact_full(
                &mut vocabulary,
                processed.as_ref(),
                &mut loader,
                json_ld::compaction::Options {
                    processing_mode: json_ld::ProcessingMode::JsonLd1_1,
                    compact_to_relative: true,
                    compact_arrays: true,
                    ordered: true,
                },
            )
            .await?;

        println!("output: {}", compacted.pretty_print());
    }

    Ok(())
}
