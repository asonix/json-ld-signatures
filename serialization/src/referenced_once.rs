use crate::{
    json_value::Map,
    rdf::{get_subject, subject_str, QuadSubject},
    subject_entry::SubjectEntry,
};
use rdf_types::Vocabulary;
use std::{cell::RefCell, collections::HashMap, rc::Rc};

#[derive(Debug, Default)]
pub(crate) struct ReferencedOnce {
    value: HashMap<String, ReferencedEntry>,
}

#[derive(Debug)]
pub(crate) enum ReferencedEntry {
    False,
    Value(ReferencedValue),
}

#[derive(Debug, Clone)]
pub(crate) struct ReferencedValue {
    pub(crate) node: Rc<RefCell<SubjectEntry>>,
    pub(crate) property: String,
    pub(crate) value: Rc<RefCell<Map>>,
}

impl ReferencedOnce {
    pub(crate) fn new() -> Self {
        Self::default()
    }

    pub(crate) fn get_mut<'a>(&'a mut self, object: &str) -> Option<&'a mut ReferencedEntry> {
        self.value.get_mut(object)
    }

    pub(crate) fn get_value(&self, object: &str) -> Option<&ReferencedValue> {
        self.value.get(object).and_then(|entry| match entry {
            ReferencedEntry::False => None,
            ReferencedEntry::Value(value) => Some(value),
        })
    }

    pub(crate) fn contains_key(&self, object: &str) -> bool {
        self.get_value(object).is_some()
    }

    pub(crate) fn insert(&mut self, object: String, referenced_value: ReferencedValue) {
        self.value
            .insert(object, ReferencedEntry::Value(referenced_value));
    }
}

impl ReferencedEntry {
    pub(crate) fn set_false(&mut self) {
        *self = Self::False;
    }
}

impl ReferencedValue {
    pub(crate) fn new<N>(
        node: Rc<RefCell<SubjectEntry>>,
        property: &QuadSubject<N>,
        value: Rc<RefCell<Map>>,
        vocabulary: &N,
    ) -> Self
    where
        N: Vocabulary,
    {
        let property = get_subject(property, vocabulary);
        let property = subject_str(&property).to_string();

        Self {
            node,
            property,
            value,
        }
    }
}
