use json_ld::{
    rdf::{
        RdfDirection, RDF_DIRECTION, RDF_FIRST, RDF_JSON, RDF_NIL, RDF_REST, RDF_TYPE, RDF_VALUE,
        XSD_BOOLEAN, XSD_DOUBLE, XSD_INTEGER, XSD_STRING,
    },
    ValidId as Subject,
};
use json_ld_syntax::Parse;
use locspan::Meta;
use rdf_types::{Term, Vocabulary};
use smallvec::SmallVec;
use std::{cell::RefCell, hash::Hash, rc::Rc};

mod compound_literal_subjects;
mod graph_map;
mod input_dataset;
mod json_value;
mod object_entry;
mod rdf;
mod referenced_once;
mod subject_entry;

use compound_literal_subjects::CompoundLiteralSubjects;
use graph_map::{GraphEntry, GraphMap};
use input_dataset::InputDataset;
use json_value::{subject_json, Array, JsonValue, Map, WithMeta};
use object_entry::ObjectEntry;
use rdf::{
    expect_iri, get_iri, subject_matches, subject_string, NormalizingQuad, QuadSubject, QuadValue,
    RDF_LANGUAGE, RDF_LIST,
};
use referenced_once::{ReferencedOnce, ReferencedValue};
use subject_entry::SubjectEntry;

#[derive(Debug)]
pub struct InvalidJson;

impl std::fmt::Display for InvalidJson {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Invalid Json")
    }
}

impl std::error::Error for InvalidJson {}

pub fn rdf_to_json_ld<N, M>(
    rdf_dataset: Vec<NormalizingQuad<N>>,
    meta: M,
    ordered: bool,
    rdf_direction: Option<RdfDirection>,
    use_native_types: bool,
    use_rdf_type: bool,
    vocabulary: &N,
) -> Result<Meta<json_ld_syntax::Value<M>, M>, InvalidJson>
where
    N: Vocabulary,
    N::Iri: Hash + Eq + Clone,
    N::BlankId: Hash + Eq + Clone,
    M: Eq + Clone,
{
    let input_dataset = InputDataset::<N>::new(rdf_dataset);

    // step 1
    let default_graph = Rc::new(RefCell::new(GraphEntry::default()));

    // step 2
    let mut graph_map = GraphMap::default();
    graph_map.insert("@default".into(), default_graph.clone());

    // step 3
    let mut referenced_once = ReferencedOnce::new();

    // step 4
    let mut compound_literal_subjects = CompoundLiteralSubjects::default();

    // step 5
    for (graph, triples) in input_dataset.graphs() {
        // step 5.1
        let name = if let Some(graph) = graph {
            subject_string(graph, vocabulary)
        } else {
            "@default".to_string()
        };

        // step 5.2
        let node_map: Rc<RefCell<GraphEntry>> = graph_map
            .entry(name.clone())
            .or_insert_with(|| Rc::new(RefCell::new(GraphEntry::default())))
            .clone();

        // step 5.3
        let compound_map = compound_literal_subjects
            .graph_entry(name.clone())
            .or_default();

        // step 5.4
        if name != "@default" && !default_graph.borrow().contains_graph(&name) {
            default_graph.borrow_mut().insert_graph(name.clone());
        }

        // step 5.5 no-op: get node_map

        // step 5.6 no-op: get compound_map

        // step 5.7
        for triple in triples {
            // step 5.7.1
            let node = node_map
                .borrow_mut()
                .subject_entry(triple.subject(), vocabulary)
                .or_insert_with(|| {
                    Rc::new(RefCell::new(SubjectEntry::new(
                        triple.subject(),
                        vocabulary,
                    )))
                })
                .clone();

            // step 5.7.2 no-op: get node

            // step 5.7.3
            if rdf_direction == Some(RdfDirection::CompoundLiteral)
                && subject_matches(triple.predicate(), RDF_DIRECTION, vocabulary)
            {
                compound_map.insert(triple.subject(), vocabulary);
            }

            // step 5.7.4
            if let Some(object) = subject_object::<N>(triple.object()) {
                node_map
                    .borrow_mut()
                    .object_entry(&object, vocabulary)
                    .or_insert_with(|| {
                        Rc::new(RefCell::new(ObjectEntry::new(&object, vocabulary)))
                    });
            }

            // step 5.7.5
            if subject_matches(triple.predicate(), RDF_TYPE, vocabulary) && !use_rdf_type {
                if let Some(object) = subject_object::<N>(triple.object()) {
                    if node.borrow().contains_type(&object, vocabulary) {
                        node.borrow_mut().insert_type(&object, vocabulary);
                    }

                    continue;
                }
            }

            // step 5.7.6
            let value = rdf_to_object(
                triple.object().clone(),
                meta.clone(),
                rdf_direction,
                use_native_types,
                vocabulary,
            )?;

            // step 5.7.7
            node.borrow_mut()
                .predicate_entry(triple.predicate(), vocabulary)
                .or_default();

            // step 5.7.8
            if !node
                .borrow()
                .contains_predicate_value(triple.predicate(), vocabulary, &value)
            {
                node.borrow_mut().insert_predicate_value(
                    triple.predicate(),
                    vocabulary,
                    value.clone(),
                );
            }

            if let Some(object) = subject_object::<N>(triple.object()) {
                // step 5.7.9
                if subject_matches(&object, RDF_NIL, vocabulary) {
                    // step 5.7.9.1
                    let mut node_map = node_map.borrow_mut();
                    let usages = node_map.usages_entry();

                    // step 5.7.9.2
                    if let Some(usages) = &mut *usages.borrow_mut() {
                        usages.push(ReferencedValue::new(
                            node,
                            triple.predicate(),
                            value,
                            vocabulary,
                        ));
                    }
                    drop(node_map);
                } else if let Some(value) =
                    referenced_once.get_mut(&subject_string(&object, vocabulary))
                {
                    // step 5.7.10
                    value.set_false();
                } else if matches!(triple.object(), json_ld::rdf::Value::Blank(_)) {
                    // step 5.7.11, 5.7.11.1
                    referenced_once.insert(
                        subject_string(&object, vocabulary),
                        ReferencedValue::new(node, triple.predicate(), value, vocabulary),
                    )
                }
            }
        }
    }

    // step 6
    for (name, graph_object) in graph_map.iter() {
        // step 6.1
        let Some(compound_map) = compound_literal_subjects.graph(name) else { continue };

        for cl in compound_map.iter() {
            // step 6.1.1
            let Some(cl_entry) = referenced_once.get_value(cl) else {
                continue;
            };

            // step 6.1.2
            let node = &cl_entry.node;

            // step 6.1.3
            let property = &cl_entry.property;

            // step 6.1.4, this seems to be ignored
            // let value = &cl_entry.value;

            // step 6.1.5
            let Some(cl_node) = graph_object.borrow_mut().remove_subject(cl) else {
                continue;
            };

            let node = node.borrow();

            // step 6.1.6
            let Some(property_array) = node.predicate_value(property) else {
                eprintln!("Expected array from node's property");
                debug_assert!(false, "Expected array from node's property");
                continue;
            };

            for cl_reference in property_array.as_slice() {
                // step 6.1.6.1
                let Some(id) = cl_reference.borrow().get("@id").cloned() else {
                    eprintln!("Expected string from @id");
                    debug_assert!(false, "Expected string from @id");
                    continue;
                };

                if !id.matches_string(cl) {
                    continue;
                }
                drop(id);

                cl_reference.borrow_mut().remove("@id");

                // step 6.1.6.2
                let at_value: JsonValue = cl_node
                    .borrow()
                    .predicate_value(RDF_VALUE.as_str())
                    .and_then(|value| value.get(0)?.borrow().get("@value").cloned())
                    .unwrap_or_else(|| ().into());

                cl_reference.borrow_mut().insert("@value".into(), at_value);

                // step 6.1.6.3
                let language: Option<JsonValue> = cl_node
                    .borrow()
                    .predicate_value(RDF_LANGUAGE.as_str())
                    .and_then(|value| value.get(0)?.borrow().get("@value").cloned());

                if let Some(language) = language {
                    cl_reference
                        .borrow_mut()
                        .insert("@language".into(), language);
                }
            }
        }

        // step 6.2, 6.3
        let usages = graph_object.borrow().usages().clone();

        // step 6.4
        if let Some(usages) = &*usages.borrow() {
            for usage in usages.iter() {
                // step 6.4.1
                let mut node = usage.node.clone();
                let mut property = usage.property.clone();
                let mut head = usage.value.clone();

                // step 6.4.2
                let mut list = Array::new();
                let mut list_nodes = Vec::new();

                // 6.4.3
                while {
                    let node = node.borrow();
                    property == RDF_REST.as_str()
                        && node.is_blank_node()
                        && referenced_once.contains_key(node.id())
                        && node
                            .predicate_value(RDF_FIRST.as_str())
                            .map(|first| first.len() == 1)
                            .unwrap_or(false)
                        && node
                            .predicate_value(RDF_REST.as_str())
                            .map(|first| first.len() == 1)
                            .unwrap_or(false)
                        && node
                            .ty()
                            .and_then(|ty| {
                                let first = ty.get(0)?;
                                Some(ty.len() == 1 && first.as_str() == RDF_LIST.as_str())
                            })
                            .unwrap_or(true)
                        && node.predicate_len() == 2
                } {
                    // step 6.4.3.1
                    list.push(
                        node.borrow()
                            .predicate_value(RDF_REST.as_str())
                            .expect("verified this key exists already")
                            .iter()
                            .map(|map| map.clone().into())
                            .collect::<Vec<JsonValue>>()
                            .into(),
                    );

                    // step 6.4.3.2
                    list_nodes.push(node.borrow().id().to_string());

                    // step 6.4.3.3
                    let node_usage = referenced_once
                        .get_value(node.borrow().id())
                        .expect("Already verified referenced_once contains node id")
                        .clone();

                    // step 6.4.3.4
                    node = node_usage.node;
                    property = node_usage.property;
                    head = node_usage.value;

                    // step 6.4.3.4
                    if !node.borrow().is_blank_node() {
                        break;
                    }
                }

                // step 6.4.4
                head.borrow_mut().remove("@id");

                // step 6.4.5
                list.reverse();

                // step 6.4.6
                head.borrow_mut().insert("@list".into(), list.into());

                // step 6.4.7
                for node_id in list_nodes {
                    graph_object.borrow_mut().remove(&node_id);
                }
            }
        };
    }

    // step 7
    let mut result = Array::new();

    // step 8
    let mut default_graph: Vec<_> = default_graph.borrow().to_vec();

    if ordered {
        default_graph.sort_by_key(|tup| tup.0.clone());
    }

    // step 8.1
    for (subject, node) in default_graph {
        let Some(node) = node.to_map() else { continue; };

        if let Some(entry) = graph_map.get(&subject) {
            // step 8.1.1
            let array = Rc::new(RefCell::new(Array::new()));
            node.borrow_mut()
                .insert("@graph".into(), array.clone().into());

            // step 8.1.2
            let mut s_n: Vec<_> = entry.borrow().to_vec();

            if ordered {
                s_n.sort_by_key(|tup| tup.0.clone());
            }

            for (_, n) in s_n {
                let only_contains_id = n
                    .get_map_mut()
                    .map(|mut map| {
                        map.remove("usages");
                        map.len() == 1 && map.contains_key("@id")
                    })
                    .unwrap_or(false);

                if !only_contains_id {
                    array.borrow_mut().push(n.clone());
                }
            }
        }

        // step 8.2
        node.borrow_mut().remove("usages");

        if node.borrow().len() > 1 || !node.borrow().contains_key("@id") {
            result.push(node.into());
        }
    }

    // step 9
    let result: JsonValue = result.into();
    Ok(WithMeta(result, meta).into())
}

fn subject_object<N>(object: &QuadValue<N>) -> Option<QuadSubject<N>>
where
    N: Vocabulary,
    N::BlankId: Clone,
    N::Iri: Clone,
{
    match object {
        Term::Iri(iri) => Some(Subject::Iri(iri.clone())),
        Term::Blank(blank) => Some(Subject::Blank(blank.clone())),
        Term::Literal(_) => None,
    }
}

fn rdf_to_object<N, M>(
    value: QuadValue<N>,
    meta: M,
    rdf_direction: Option<RdfDirection>,
    use_native_types: bool,
    vocabulary: &N,
) -> Result<Rc<RefCell<Map>>, InvalidJson>
where
    N: Vocabulary,
    N::Iri: Eq + Clone,
    M: Clone,
{
    match value {
        // step 1
        Term::Iri(iri) => {
            let mut map = Map::new();
            map.insert("@id".into(), subject_json(&Subject::Iri(iri), vocabulary));
            Ok(Rc::new(RefCell::new(map)))
        }
        Term::Blank(blank) => {
            let mut map = Map::new();
            map.insert(
                "@id".into(),
                subject_json(&Subject::Blank(blank), vocabulary),
            );
            Ok(Rc::new(RefCell::new(map)))
        }

        // step 2
        Term::Literal(literal) => {
            // step 2.1
            let mut result = Map::new();

            // step 2.2
            let converted_value: JsonValue;

            // step 2.3
            let ty: Option<json_syntax::String>;

            (converted_value, ty) = match literal {
                // step 2.4
                // step 2.4.1
                rdf_types::Literal::TypedString(value, value_ty)
                    if use_native_types && expect_iri(&value_ty, XSD_STRING, vocabulary) =>
                {
                    let value_ty = get_iri(&value_ty, vocabulary);

                    let value_ty: json_syntax::String = value_ty.as_str().into();
                    (value.as_str().into(), Some(value_ty))
                }
                // step 2.4.2
                rdf_types::Literal::TypedString(value, value_ty)
                    if use_native_types && expect_iri(&value_ty, XSD_BOOLEAN, vocabulary) =>
                {
                    let value_ty = get_iri(&value_ty, vocabulary);

                    if value.as_str() == "true" {
                        (true.into(), None)
                    } else if value.as_str() == "false" {
                        (false.into(), None)
                    } else {
                        (value.as_str().into(), Some(value_ty.as_str().into()))
                    }
                }
                // step 2.4.3
                rdf_types::Literal::TypedString(value, value_ty)
                    if use_native_types
                        && (expect_iri(&value_ty, XSD_INTEGER, vocabulary)
                            || expect_iri(&value_ty, XSD_DOUBLE, vocabulary)) =>
                {
                    let value_ty = get_iri(&value_ty, vocabulary);

                    if let Ok(number_buf) =
                        json_number::SmallNumberBuf::new(SmallVec::from_slice(value.as_bytes()))
                    {
                        (number_buf.into(), Some(value_ty.as_str().into()))
                    } else {
                        (value.as_str().into(), Some(value_ty.as_str().into()))
                    }
                }

                // step 2.5
                rdf_types::Literal::TypedString(value, value_ty)
                    if expect_iri(&value_ty, RDF_JSON, vocabulary) =>
                {
                    let value = json_syntax::Value::parse_str(value.as_str(), |_| meta.clone())
                        .map_err(|_| InvalidJson)?;

                    let value: JsonValue = value.into();

                    (value, Some(get_iri(&value_ty, vocabulary).as_str().into()))
                }

                // step 2.6
                rdf_types::Literal::TypedString(value, value_ty)
                    if get_iri(&value_ty, vocabulary)
                        .as_str()
                        .starts_with("https://www.w3.org/ns/i18n#")
                        && rdf_direction == Some(RdfDirection::I18nDatatype) =>
                {
                    // step 2.6.2
                    let iri = get_iri(&value_ty, vocabulary);
                    let lang = iri
                        .as_str()
                        .trim_start_matches("https://www.w3.org/ns/i18n#");
                    let (lang, direction) = lang.split_once('_').expect("Invalid language");

                    if !lang.is_empty() {
                        // TODO: Validate lang
                        result.insert("@language".into(), lang.into());
                    }

                    // step 2.6.3
                    if !direction.is_empty() {
                        result.insert("@direction".into(), direction.into());
                    }

                    // step 2.6.1
                    (value.as_str().into(), None)
                }
                // step 2.7
                rdf_types::Literal::LangString(value, language_tag_buf) => {
                    result.insert("@language".into(), language_tag_buf.as_str().into());

                    (value.as_str().into(), None)
                }

                // step 2.8
                rdf_types::Literal::TypedString(value, value_ty)
                    if !expect_iri(&value_ty, XSD_STRING, vocabulary) =>
                {
                    let value_ty = get_iri(&value_ty, vocabulary);

                    (value.as_str().into(), Some(value_ty.as_str().into()))
                }

                // step 2.2, all remaining matches
                rdf_types::Literal::TypedString(value, _) => (value.as_str().into(), None),
                rdf_types::Literal::String(value) => (value.as_str().into(), None),
            };

            // step 2.9
            result.insert("@value".into(), converted_value);

            // step 2.10
            if let Some(ty) = ty {
                result.insert("@type".into(), ty.into());
            }

            Ok(Rc::new(RefCell::new(result)))
        }
    }
}
