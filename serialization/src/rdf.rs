use iref::Iri;
use json_ld::ValidId as Subject;
use rdf_types::{BlankId, BlankIdVocabulary, IriVocabulary, Quad, Vocabulary};
use static_iref::iri;

pub(crate) const RDF_LANGUAGE: Iri<'static> =
    iri!("http://www.w3.org/1999/02/22-rdf-syntax-ns#language");
pub(crate) const RDF_LIST: Iri<'static> = iri!("http://www.w3.org/1999/02/22-rdf-syntax-ns#list");

pub(crate) type QuadSubject<N> =
    Subject<<N as IriVocabulary>::Iri, <N as BlankIdVocabulary>::BlankId>;

pub(crate) type QuadValue<N> =
    json_ld::rdf::Value<<N as IriVocabulary>::Iri, <N as BlankIdVocabulary>::BlankId>;

pub(crate) type NormalizingQuad<N> =
    Quad<QuadSubject<N>, QuadSubject<N>, QuadValue<N>, QuadSubject<N>>;

pub(crate) fn expect_iri<N>(id: &N::Iri, iri: Iri<'_>, vocabulary: &N) -> bool
where
    N: Vocabulary,
    N::Iri: Eq,
{
    vocabulary.get(iri).map(|iri| iri == *id).unwrap_or(false) || get_iri(id, vocabulary) == iri
}

pub(crate) fn get_iri<'a, N>(id: &'a N::Iri, vocabulary: &'a N) -> Iri<'a>
where
    N: Vocabulary,
{
    vocabulary.iri(id).expect("Id in vocabulary")
}

pub(crate) fn get_subject<'a, N>(
    id: &'a QuadSubject<N>,
    vocabulary: &'a N,
) -> Subject<Iri<'a>, &'a BlankId>
where
    N: Vocabulary,
{
    match id {
        Subject::Iri(iri) => Subject::Iri(vocabulary.iri(iri).expect("Id in vocabulary")),
        Subject::Blank(blank) => {
            Subject::Blank(vocabulary.blank_id(blank).expect("Id in vocabulary"))
        }
    }
}

pub(crate) fn subject_str<'a>(subject: &'a Subject<Iri<'a>, &'a BlankId>) -> &'a str {
    match subject {
        Subject::Iri(ref iri) => iri.as_str(),
        Subject::Blank(blank) => blank.as_str(),
    }
}

pub(crate) fn subject_matches<N>(subject: &QuadSubject<N>, iri: Iri<'_>, vocabulary: &N) -> bool
where
    N: Vocabulary,
    N::Iri: Eq,
{
    match subject {
        Subject::Iri(id) => expect_iri(id, iri, vocabulary),
        Subject::Blank(_) => false,
    }
}

pub(crate) fn subject_string<N>(id: &QuadSubject<N>, vocabulary: &N) -> String
where
    N: Vocabulary,
{
    let subject = get_subject(id, vocabulary);
    let s = subject_str(&subject);
    s.to_string()
}
