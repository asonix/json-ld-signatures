use crate::rdf::{get_subject, subject_str, QuadSubject};
use indexmap::IndexMap;
use locspan::Meta;
use rdf_types::Vocabulary;
use std::{
    cell::{RefCell, RefMut},
    rc::Rc,
};

pub(crate) fn subject_json<N>(id: &QuadSubject<N>, vocabulary: &N) -> JsonValue
where
    N: Vocabulary,
{
    let subject = get_subject(id, vocabulary);
    let s = subject_str(&subject);
    s.into()
}

pub(crate) type Map = IndexMap<String, JsonValue>;
pub(crate) type Array = Vec<JsonValue>;

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct JsonValue {
    value: JsonValueKind,
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum JsonValueKind {
    Map(Rc<RefCell<Map>>),
    Array(Rc<RefCell<Array>>),
    String(json_syntax::String),
    Number(json_syntax::NumberBuf),
    Boolean(bool),
    Null,
}

impl JsonValue {
    pub(crate) fn matches_string(&self, s: &str) -> bool {
        self.get_string().map(|string| string == s).unwrap_or(false)
    }

    pub(crate) fn get_string(&self) -> Option<&str> {
        match self.value {
            JsonValueKind::String(ref s) => Some(s.as_str()),
            _ => None,
        }
    }

    pub(crate) fn get_array_mut(&self) -> Option<RefMut<'_, Array>> {
        match self.value {
            JsonValueKind::Array(ref array) => Some(array.borrow_mut()),
            _ => None,
        }
    }

    pub(crate) fn get_map_mut(&self) -> Option<RefMut<'_, Map>> {
        match self.value {
            JsonValueKind::Map(ref map) => Some(map.borrow_mut()),
            _ => None,
        }
    }

    pub(crate) fn to_map(&self) -> Option<Rc<RefCell<Map>>> {
        match self.value {
            JsonValueKind::Map(ref map) => Some(map.clone()),
            _ => None,
        }
    }
}

impl From<Map> for JsonValue {
    fn from(value: Map) -> Self {
        JsonValue {
            value: JsonValueKind::Map(Rc::new(RefCell::new(value))),
        }
    }
}

impl From<Rc<RefCell<Map>>> for JsonValue {
    fn from(value: Rc<RefCell<Map>>) -> Self {
        JsonValue {
            value: JsonValueKind::Map(value),
        }
    }
}

impl From<Array> for JsonValue {
    fn from(value: Array) -> Self {
        JsonValue {
            value: JsonValueKind::Array(Rc::new(RefCell::new(value))),
        }
    }
}

impl From<Rc<RefCell<Array>>> for JsonValue {
    fn from(value: Rc<RefCell<Array>>) -> Self {
        JsonValue {
            value: JsonValueKind::Array(value),
        }
    }
}

impl<'a> From<&'a str> for JsonValue {
    fn from(value: &'a str) -> Self {
        JsonValue {
            value: JsonValueKind::String(value.into()),
        }
    }
}

impl From<json_syntax::String> for JsonValue {
    fn from(value: json_syntax::String) -> Self {
        JsonValue {
            value: JsonValueKind::String(value),
        }
    }
}

impl From<json_syntax::NumberBuf> for JsonValue {
    fn from(value: json_syntax::NumberBuf) -> Self {
        JsonValue {
            value: JsonValueKind::Number(value),
        }
    }
}

impl From<bool> for JsonValue {
    fn from(value: bool) -> Self {
        JsonValue {
            value: JsonValueKind::Boolean(value),
        }
    }
}

impl From<()> for JsonValue {
    fn from(_: ()) -> Self {
        JsonValue {
            value: JsonValueKind::Null,
        }
    }
}

impl<M> From<Meta<json_syntax::Value<M>, M>> for JsonValue {
    fn from(Meta(value, _): Meta<json_syntax::Value<M>, M>) -> Self {
        match value {
            json_syntax::Value::Null => ().into(),
            json_syntax::Value::Boolean(boolean) => boolean.into(),
            json_syntax::Value::Number(num) => num.into(),
            json_ld_syntax::Value::String(string) => string.into(),
            json_ld_syntax::Value::Array(array) => array.into(),
            json_ld_syntax::Value::Object(object) => object.into(),
        }
    }
}

pub(crate) struct WithMeta<M>(pub(crate) JsonValue, pub(crate) M);

impl<M> From<WithMeta<M>> for Meta<json_syntax::Value<M>, M>
where
    M: Clone,
{
    fn from(WithMeta(value, meta): WithMeta<M>) -> Self {
        let value = match value.value {
            JsonValueKind::Null => json_syntax::Value::Null,
            JsonValueKind::Boolean(boolean) => json_syntax::Value::Boolean(boolean),
            JsonValueKind::Number(number) => json_syntax::Value::Number(number),
            JsonValueKind::String(string) => json_syntax::Value::String(string),
            JsonValueKind::Array(array) => json_syntax::Value::Array(
                array
                    .borrow()
                    .iter()
                    .map(|value| WithMeta(value.clone(), meta.clone()).into())
                    .collect(),
            ),
            JsonValueKind::Map(map) => json_syntax::Value::Object(map.borrow().iter().fold(
                json_syntax::Object::new(),
                |mut object, (k, v)| {
                    object.insert(
                        Meta(k.as_str().into(), meta.clone()),
                        WithMeta(v.clone(), meta.clone()).into(),
                    );
                    object
                },
            )),
        };

        Meta(value, meta)
    }
}

impl<M> From<json_syntax::Array<M>> for JsonValue {
    fn from(values: json_syntax::Array<M>) -> Self {
        let array = values
            .into_iter()
            .map(|value| value.into())
            .collect::<Vec<JsonValue>>();
        array.into()
    }
}

impl<M> From<json_syntax::Object<M>> for JsonValue {
    fn from(value: json_syntax::Object<M>) -> Self {
        let mut map = Map::new();

        for json_syntax::object::Entry { key, value } in value.into_iter() {
            if let Some(entry) = map.remove(key.0.as_str()) {
                let entry_clone = entry.clone();

                let entry = if let Some(mut array) = entry.get_array_mut() {
                    array.push(value.into());
                    drop(array);
                    entry_clone
                } else {
                    vec![entry_clone, value.into()].into()
                };

                map.insert(key.0.to_string(), entry);
            } else {
                map.insert(key.0.to_string(), value.into());
            }
        }

        map.into()
    }
}
