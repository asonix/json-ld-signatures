use crate::{
    json_value::{JsonValue, Map},
    rdf::{get_subject, subject_str, QuadSubject},
};
use rdf_types::Vocabulary;

pub(crate) struct ObjectEntry {
    // short for
    // @id -> rdf:nil
    id: json_syntax::String,
}

impl ObjectEntry {
    pub(crate) fn new<N>(object: &QuadSubject<N>, vocabulary: &N) -> Self
    where
        N: Vocabulary,
    {
        let object = get_subject(object, vocabulary);
        let object_str = subject_str(&object);

        ObjectEntry {
            id: object_str.into(),
        }
    }

    pub(crate) fn to_json_value(&self) -> JsonValue {
        let mut map = Map::new();
        map.insert("@id".into(), self.id.clone().into());
        map.into()
    }
}
