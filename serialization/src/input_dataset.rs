use crate::rdf::{NormalizingQuad, QuadSubject, QuadValue};
use rdf_types::{Triple, Vocabulary};
use std::{collections::HashMap, hash::Hash};

type SerializingTriple<N> = Triple<QuadSubject<N>, QuadSubject<N>, QuadValue<N>>;

pub(crate) struct InputDataset<N>
where
    N: Vocabulary,
{
    graphs: HashMap<Option<QuadSubject<N>>, Vec<SerializingTriple<N>>>,
}

impl<N> Default for InputDataset<N>
where
    N: Vocabulary,
{
    fn default() -> Self {
        Self {
            graphs: Default::default(),
        }
    }
}

impl<N> InputDataset<N>
where
    N: Vocabulary,
{
    pub(crate) fn new(input_dataset: Vec<NormalizingQuad<N>>) -> Self
    where
        N::Iri: Hash + Eq,
        N::BlankId: Hash + Eq,
    {
        input_dataset
            .into_iter()
            .fold(Self::default(), |mut this, quad| {
                let (subject, predicate, object, graph) = quad.into_parts();

                let triple = Triple(subject, predicate, object);

                this.graphs.entry(graph).or_default().push(triple);

                this
            })
    }

    pub(crate) fn graphs(
        &self,
    ) -> impl Iterator<Item = (Option<&'_ QuadSubject<N>>, &'_ [SerializingTriple<N>])> {
        self.graphs.iter().map(|(k, v)| (k.as_ref(), v.as_slice()))
    }
}
