use crate::rdf::subject_string;
use indexmap::{IndexMap, IndexSet};
use json_ld::ValidId as Subject;
use rdf_types::Vocabulary;

#[derive(Default)]
pub(crate) struct CompoundLiteralSubjects {
    graphs: IndexMap<String, CompoundMap>,
}

impl CompoundLiteralSubjects {
    pub(crate) fn graph_entry(
        &mut self,
        graph: String,
    ) -> indexmap::map::Entry<'_, String, CompoundMap> {
        self.graphs.entry(graph)
    }

    pub(crate) fn graph(&self, graph: &str) -> Option<&CompoundMap> {
        self.graphs.get(graph)
    }
}

#[derive(Default)]
pub(crate) struct CompoundMap {
    subjects: IndexSet<String>,
}

impl CompoundMap {
    pub(crate) fn insert<N>(&mut self, subject: &Subject<N::Iri, N::BlankId>, vocabulary: &N)
    where
        N: Vocabulary,
    {
        let subject = subject_string(subject, vocabulary);

        self.subjects.insert(subject);
    }

    pub(crate) fn iter(&self) -> indexmap::set::Iter<'_, String> {
        self.subjects.iter()
    }
}
