use crate::{
    json_value::{JsonValue, Map},
    rdf::{get_subject, subject_str, subject_string, QuadSubject},
};
use indexmap::IndexMap;
use rdf_types::Vocabulary;
use std::{cell::RefCell, rc::Rc};

#[derive(Debug)]
pub(crate) struct SubjectEntry {
    // @id -> {subject}
    id: json_syntax::String,
    // @type -> Array
    //   {object} (if type)
    ty: Option<Vec<json_syntax::String>>,
    // {predicate} -> Array
    //   {object} (value)
    predicate_values: IndexMap<String, Vec<Rc<RefCell<Map>>>>,
}

impl SubjectEntry {
    pub(crate) fn new<N>(subject: &QuadSubject<N>, vocabulary: &N) -> Self
    where
        N: Vocabulary,
    {
        let subject = get_subject(subject, vocabulary);
        let subject_str = subject_str(&subject);

        SubjectEntry {
            id: subject_str.into(),
            ty: None,
            predicate_values: Default::default(),
        }
    }

    pub(crate) fn contains_type<N>(&self, rhs: &QuadSubject<N>, vocabulary: &N) -> bool
    where
        N: Vocabulary,
    {
        let rhs = get_subject(rhs, vocabulary);
        let rhs_str = subject_str(&rhs);

        if let Some(ty) = &self.ty {
            ty.iter().any(|lhs| lhs.as_str() == rhs_str)
        } else {
            false
        }
    }

    pub(crate) fn insert_type<N>(&mut self, ty: &QuadSubject<N>, vocabulary: &N)
    where
        N: Vocabulary,
    {
        let ty = get_subject(ty, vocabulary);
        let ty_str = subject_str(&ty);

        if let Some(ty) = &mut self.ty {
            ty.push(ty_str.into());
        } else {
            self.ty = Some(vec![ty_str.into()]);
        }
    }

    pub(crate) fn predicate_entry<'a, N>(
        &'a mut self,
        predicate: &QuadSubject<N>,
        vocabulary: &N,
    ) -> indexmap::map::Entry<'a, String, Vec<Rc<RefCell<Map>>>>
    where
        N: Vocabulary,
    {
        self.predicate_values
            .entry(subject_string(predicate, vocabulary))
    }

    pub(crate) fn contains_predicate_value<N>(
        &self,
        predicate: &QuadSubject<N>,
        vocabulary: &N,
        rhs: &Rc<RefCell<Map>>,
    ) -> bool
    where
        N: Vocabulary,
    {
        let predicate = get_subject(predicate, vocabulary);
        let predicate_str = subject_str(&predicate);

        let Some(values) = self.predicate_values.get(predicate_str) else { return false; };

        values.iter().any(|lhs| *lhs.borrow() == *rhs.borrow())
    }

    pub(crate) fn insert_predicate_value<N>(
        &mut self,
        predicate: &QuadSubject<N>,
        vocabulary: &N,
        value: Rc<RefCell<Map>>,
    ) where
        N: Vocabulary,
    {
        let predicate = get_subject(predicate, vocabulary);
        let predicate_str = subject_str(&predicate);

        let Some(values) = self.predicate_values.get_mut(predicate_str) else { return; };

        values.push(value);
    }

    pub(crate) fn predicate_value(&self, predicate: &str) -> Option<&Vec<Rc<RefCell<Map>>>> {
        self.predicate_values.get(predicate)
    }

    pub(crate) fn is_blank_node(&self) -> bool {
        self.id.starts_with("_:")
    }

    pub(crate) fn id(&self) -> &str {
        self.id.as_str()
    }

    pub(crate) fn ty(&self) -> Option<&[json_syntax::String]> {
        self.ty.as_deref()
    }

    pub(crate) fn predicate_len(&self) -> usize {
        self.predicate_values.len()
    }

    pub(crate) fn to_json_value(&self) -> JsonValue {
        let mut map = Map::new();

        map.insert("@id".into(), self.id.clone().into());

        if let Some(ty) = &self.ty {
            map.insert(
                "@type".into(),
                ty.iter()
                    .map(|ty| ty.as_str().into())
                    .collect::<Vec<JsonValue>>()
                    .into(),
            );
        }

        for (predicate, values) in &self.predicate_values {
            map.insert(
                predicate.into(),
                values
                    .iter()
                    .map(|value| value.clone().into())
                    .collect::<Vec<JsonValue>>()
                    .into(),
            );
        }

        map.into()
    }
}
