use crate::{
    json_value::{JsonValue, Map},
    object_entry::ObjectEntry,
    rdf::{subject_string, QuadSubject},
    referenced_once::ReferencedValue,
    subject_entry::SubjectEntry,
};
use indexmap::{IndexMap, IndexSet};
use rdf_types::Vocabulary;
use std::{cell::RefCell, rc::Rc};

#[derive(Default)]
pub(crate) struct GraphMap {
    graphs: IndexMap<String, Rc<RefCell<GraphEntry>>>,
}

#[derive(Default)]
pub(crate) struct GraphEntry {
    // short for
    // {graph_name} -> Map
    //   @id -> {graph_name}
    graphs: IndexSet<String>,

    // short for
    // {subject} -> Map
    //   @id -> {subject}
    //   @type -> Array
    //     {object}
    //   {predicate} -> Array
    //     {object}
    subjects: IndexMap<String, Rc<RefCell<SubjectEntry>>>,

    // short for
    // {object} -> Map
    //   @id -> {object}

    // short for
    // {object} -> Map
    //   @id -> {object}
    objects: IndexMap<String, Rc<RefCell<ObjectEntry>>>,

    // short for
    // nil -> Map
    //   usages -> Array
    //     node -> Map (SubjectEntry)
    //     property -> {predicate}
    //     value -> {object}
    usages: Rc<RefCell<Option<Vec<ReferencedValue>>>>,
}

impl GraphMap {
    pub(crate) fn insert(&mut self, graph_name: String, graph_entry: Rc<RefCell<GraphEntry>>) {
        self.graphs.insert(graph_name, graph_entry);
    }

    pub(crate) fn entry(
        &mut self,
        graph_name: String,
    ) -> indexmap::map::Entry<'_, String, Rc<RefCell<GraphEntry>>> {
        self.graphs.entry(graph_name)
    }

    pub(crate) fn iter(&self) -> indexmap::map::Iter<'_, String, Rc<RefCell<GraphEntry>>> {
        self.graphs.iter()
    }

    pub(crate) fn get(&self, graph: &str) -> Option<&Rc<RefCell<GraphEntry>>> {
        self.graphs.get(graph)
    }
}

impl GraphEntry {
    pub(crate) fn contains_graph(&self, graph_name: &str) -> bool {
        self.graphs.contains(graph_name)
    }

    pub(crate) fn insert_graph(&mut self, graph_name: String) {
        self.graphs.insert(graph_name);
    }

    pub(crate) fn subject_entry<'a, N>(
        &'a mut self,
        subject: &QuadSubject<N>,
        vocabulary: &N,
    ) -> indexmap::map::Entry<'a, String, Rc<RefCell<SubjectEntry>>>
    where
        N: Vocabulary,
    {
        self.subjects.entry(subject_string(subject, vocabulary))
    }

    pub(crate) fn remove_subject(&mut self, subject: &str) -> Option<Rc<RefCell<SubjectEntry>>> {
        self.subjects.remove(subject)
    }

    pub(crate) fn object_entry<'a, N>(
        &'a mut self,
        object: &QuadSubject<N>,
        vocabulary: &N,
    ) -> indexmap::map::Entry<'a, String, Rc<RefCell<ObjectEntry>>>
    where
        N: Vocabulary,
    {
        self.objects.entry(subject_string(object, vocabulary))
    }

    pub(crate) fn remove(&mut self, item: &str) {
        self.subjects.remove(item);
        self.objects.remove(item);
    }

    pub(crate) fn usages_entry(&mut self) -> &Rc<RefCell<Option<Vec<ReferencedValue>>>> {
        *self.usages.borrow_mut() = Some(Vec::new());
        &self.usages
    }

    pub(crate) fn usages(&self) -> &Rc<RefCell<Option<Vec<ReferencedValue>>>> {
        &self.usages
    }

    pub(crate) fn to_vec(&self) -> Vec<(String, JsonValue)> {
        let mut vec: Vec<(String, JsonValue)> = self
            .graphs
            .iter()
            .map(|graph| {
                let mut map = Map::new();
                map.insert("@id".into(), graph.as_str().into());
                (graph.into(), map.into())
            })
            .collect();

        vec.extend(self.subjects.iter().map(|(subject, subject_entry)| {
            (subject.clone(), subject_entry.borrow().to_json_value())
        }));

        vec.extend(
            self.objects.iter().map(|(object, object_entry)| {
                (object.clone(), object_entry.borrow().to_json_value())
            }),
        );

        vec
    }
}
