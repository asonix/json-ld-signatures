use contextual::WithContext;
use itertools::Itertools;
use json_ld::{RdfQuads, ValidId as Subject};
use locspan::{Location, Span};
use rdf_types::{
    generator::Blank, BlankIdVocabularyMut, RdfDisplay, Term, Vocabulary, VocabularyMut,
};
use std::{
    borrow::Cow,
    collections::{BTreeMap, HashMap, HashSet},
    hash::Hash,
};

mod input_dataset;
mod issuer;
#[cfg(feature = "openssl-sha2")]
mod openssl_impls;
mod output_dataset;
#[cfg(feature = "rustcrypto-sha2")]
mod rustcrypto_impls;

use input_dataset::{InputDataset, NormalizingQuad, Position, QuadSubject, QuadValue};
use issuer::Issuer;

pub use output_dataset::OutputDataset;

#[derive(Clone, Debug)]
pub struct Security;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct HexHash(pub String);

pub trait Sha256 {
    fn update(&mut self, bytes: &[u8]);

    fn finalize_hex_and_reset(&mut self) -> HexHash;
}

pub struct CanonicalizationState<'a, N, S>
where
    N: Vocabulary,
{
    sha256: S,

    canonical_vocabulary: &'a mut N,

    canonical_issuer: Issuer<N::BlankId>,

    blank_node_to_quads: HashMap<N::BlankId, HashSet<Position>>,
    hash_to_blank_nodes: BTreeMap<HexHash, HashSet<N::BlankId>>,
}

pub fn normalize<N, D, R, S>(
    vocabulary: &mut N,
    document_id: D,
    rdf_quads: R,
    bail_on_large_inputs: bool,
) -> Result<OutputDataset<N>, Security>
where
    R: RdfQuads<N::Iri, N::BlankId, Location<D>>,
    D: Clone,
    S: Sha256 + Default,
    N: Vocabulary + VocabularyMut + Default,
    N::Iri: Clone + Eq + Hash + Send + Sync + std::fmt::Debug,
    N::BlankId: Clone + Eq + Hash + Send + Sync + std::fmt::Debug,
{
    CanonicalizationState::<N, S>::new(vocabulary).normalize(
        document_id,
        rdf_quads,
        bail_on_large_inputs,
    )
}

impl<'a, N, S> CanonicalizationState<'a, N, S>
where
    N: Vocabulary,
{
    /// Step 1
    fn new(canonical_vocabulary: &'a mut N) -> Self
    where
        S: Default,
    {
        Self {
            sha256: S::default(),
            canonical_vocabulary,
            canonical_issuer: Issuer::default(),
            blank_node_to_quads: Default::default(),
            hash_to_blank_nodes: Default::default(),
        }
    }

    fn normalize<D, R>(
        mut self,
        document_id: D,
        rdf_quads: R,
        bail_on_large_inputs: bool,
    ) -> Result<OutputDataset<N>, Security>
    where
        R: RdfQuads<N::Iri, N::BlankId, Location<D>>,
        D: Clone,
        S: Sha256,
        N: VocabularyMut + Default,
        N::Iri: Clone + Eq + Hash + Send + Sync + std::fmt::Debug,
        N::BlankId: Clone + Eq + Hash + Send + Sync + std::fmt::Debug,
    {
        let input_dataset = self.input_dataset(document_id, rdf_quads);

        // Step 2
        self.find_blank_nodes(&input_dataset);

        // Step 3, 4, and 5
        self.issue_simple_canonical_identifiers(&input_dataset);

        // Step 6
        self.issue_complex_canonical_identifiers(bail_on_large_inputs, &input_dataset)?;

        // Step 7
        Ok(self.normalize_quads(&input_dataset))
    }

    // (preparing input dataset is not a step, but we're coming from json ld types here)
    fn input_dataset<R, D>(&mut self, document_id: D, rdf_quads: R) -> InputDataset<N>
    where
        R: RdfQuads<N::Iri, N::BlankId, Location<D>>,
        D: Clone,
        N: VocabularyMut,
        N::Iri: Clone + Eq + Hash + Send + Sync,
        N::BlankId: Clone + Eq + Hash + Send + Sync,
    {
        let mut pre_gen = Blank::new().with_metadata(Location::new(document_id, Span::default()));

        InputDataset::from_rdf_quads(rdf_quads, self.canonical_vocabulary, &mut pre_gen)
    }

    // Step 2
    fn find_blank_nodes(&mut self, input_dataset: &InputDataset<N>)
    where
        N::BlankId: Clone + Eq + Hash,
    {
        for (position, quad) in input_dataset.quads() {
            // step 2.1
            let iter = [
                subject_as_blank_id::<N>(quad.subject()),
                object_as_blank_id::<N>(quad.object()),
                quad.graph().and_then(subject_as_blank_id::<N>),
            ]
            .into_iter()
            .flatten();

            for blank_id in iter {
                self.blank_node_to_quads
                    .entry(blank_id.clone())
                    .or_default()
                    .insert(position);
            }
        }
    }

    // Step 3, 4, and 5
    fn issue_simple_canonical_identifiers(&mut self, input_dataset: &InputDataset<N>)
    where
        S: Sha256,
        N::BlankId: Clone + Eq + Hash,
        N: VocabularyMut + BlankIdVocabularyMut,
    {
        // step 3
        let mut non_normalized_identifiers: HashSet<N::BlankId> =
            self.blank_node_to_quads.keys().cloned().collect();

        // step 4
        let mut simple = true;

        // step 5
        while simple {
            // step 5.1
            simple = false;

            // step 5.2
            self.hash_to_blank_nodes.clear();

            // step 5.3
            for identifier in non_normalized_identifiers.iter() {
                // step 5.3.1
                let hash = self.hash_first_degree_quads((*identifier).clone(), input_dataset);

                // step 5.3.2
                self.hash_to_blank_nodes
                    .entry(hash)
                    .or_default()
                    .insert((*identifier).clone());
            }

            // step 5.4
            self.hash_to_blank_nodes.retain(|_, blank_id_set| {
                // step 5.4.1
                if blank_id_set.len() > 1 {
                    return true;
                }

                // this will only iterate once - it wouldn't exist  with 0 elements and wouldn't be
                // reached with more than 1
                for identifier in blank_id_set.drain() {
                    // step 5.4.3
                    non_normalized_identifiers.remove(&identifier);

                    // step 5.4.2
                    self.canonical_issuer
                        .issue_identifier(identifier, self.canonical_vocabulary);
                }

                // step 5.4.5
                simple = true;

                // step 5.4.4
                false
            });
        }
    }

    // Step 6
    fn issue_complex_canonical_identifiers(
        &mut self,
        bail_on_large_inputs: bool,
        input_dataset: &InputDataset<N>,
    ) -> Result<(), Security>
    where
        N: Default + BlankIdVocabularyMut + VocabularyMut,
        N::BlankId: Clone + Eq + Hash,
        S: Sha256,
    {
        let hash_to_blank_nodes = std::mem::take(&mut self.hash_to_blank_nodes);

        for (_, identifier_list) in hash_to_blank_nodes {
            // step 6.1
            let mut hash_path_list = Vec::new();

            // step 6.2
            for identifier in identifier_list {
                // step 6.2.1
                if self.canonical_issuer.contains(&identifier) {
                    continue;
                }

                if bail_on_large_inputs {
                    return Err(Security);
                }

                // step 6.2.2
                let mut temporary_issuer = Issuer::new_with_prefix("b");

                // step 6.2.3
                let mut temporary_vocabulary = N::default();

                temporary_issuer.issue_identifier(identifier.clone(), &mut temporary_vocabulary);

                // step 6.2.4
                let hash = self.hash_n_degree_quads(
                    &mut temporary_vocabulary,
                    &mut temporary_issuer,
                    identifier,
                    input_dataset,
                );

                hash_path_list.push((hash, temporary_issuer));
            }

            // step 6.3
            for (_, temporary_issuer) in hash_path_list {
                // step 6.3.1
                for (existing_identifier, _) in temporary_issuer.iter() {
                    self.canonical_issuer
                        .issue_identifier(existing_identifier.clone(), self.canonical_vocabulary);
                }
            }
        }

        Ok(())
    }

    // Step 7
    fn normalize_quads(&self, input_dataset: &InputDataset<N>) -> OutputDataset<N>
    where
        N::BlankId: Eq + Hash + Clone,
        N::Iri: Clone,
    {
        let quads = input_dataset
            .quads()
            .filter_map(|(_, quad)| {
                // step 7.1
                let subject = self.translate_subject(quad.subject())?;

                let predicate = quad.predicate().clone();

                let object = self.translate_object(quad.object())?;

                let graph = if let Some(graph) = quad.graph() {
                    Some(self.translate_subject(graph)?)
                } else {
                    None
                };

                Some(rdf_types::Quad(subject, predicate, object, graph))
            })
            .collect();

        OutputDataset::new(quads, self.canonical_vocabulary)
    }

    fn translate_object(&self, object: &QuadValue<N>) -> Option<QuadValue<N>>
    where
        N::BlankId: Eq + Hash + Clone,
        N::Iri: Clone,
    {
        match object {
            Term::Iri(iri) => Some(Term::Iri(iri.clone())),
            Term::Blank(blank) => Some(Term::Blank(self.canonical_issuer.get(blank)?.clone())),
            Term::Literal(literal) => Some(Term::Literal(literal.clone())),
        }
    }

    fn translate_subject(&self, subject: &QuadSubject<N>) -> Option<QuadSubject<N>>
    where
        N::BlankId: Eq + Hash + Clone,
        N::Iri: Clone,
    {
        match subject {
            Subject::Iri(iri) => Some(Subject::Iri(iri.clone())),
            Subject::Blank(blank) => {
                Some(Subject::Blank(self.canonical_issuer.get(blank)?.clone()))
            }
        }
    }

    fn hash_n_degree_quads(
        &mut self,
        vocabulary: &mut N,
        issuer: &mut Issuer<N::BlankId>,
        identifier: N::BlankId,
        input_dataset: &InputDataset<N>,
    ) -> HexHash
    where
        N: VocabularyMut,
        N::BlankId: Clone + Eq + Hash,
        S: Sha256,
    {
        // step 1
        let mut hash_to_related_blank_nodes: HashMap<HexHash, HashSet<N::BlankId>> = HashMap::new();

        // step 2
        if let Some(quad_positions) = self.blank_node_to_quads.get(&identifier).cloned() {
            // step 3
            for quad_position in quad_positions {
                let quad = input_dataset
                    .get(quad_position)
                    .expect("Positions are created from the input dataset");

                // step 3.1
                let iter = [
                    ("s", subject_as_blank_id::<N>(quad.subject())),
                    ("o", object_as_blank_id::<N>(quad.object())),
                    ("g", quad.graph().and_then(subject_as_blank_id::<N>)),
                ]
                .into_iter()
                .filter_map(|(position, opt)| Some((position, opt?)))
                .filter(|(_, blank_id)| identifier != **blank_id);

                for (position, related) in iter {
                    // step 3.1.1
                    let hash = self.hash_related_blank_node(
                        issuer,
                        vocabulary,
                        related,
                        quad,
                        position,
                        input_dataset,
                    );

                    // step 3.1.2
                    hash_to_related_blank_nodes
                        .entry(hash)
                        .or_default()
                        .insert(related.clone());
                }
            }
        } else {
            eprintln!("No quad positions");
        }

        // step 4
        let mut data_to_hash = String::new();

        // step 5
        for (related_hash, blank_node_list) in hash_to_related_blank_nodes {
            // step 5.1
            data_to_hash += &related_hash.0;

            // step 5.2
            let mut chosen_path = String::new();

            // step 5.3
            let mut chosen_issuer = Default::default();

            'permute: for permutation in permute(blank_node_list) {
                // step 5.4.1
                let mut issuer_copy = issuer.clone();
                // step 5.4.2
                let mut path = String::new();
                // step 5.4.3
                let mut recursion_list = HashSet::new();

                // step 5.4.4
                for related in permutation {
                    if let Some(blank) = self.canonical_issuer.get(&related) {
                        // step 5.4.4.1
                        if let Some(blank_id) = self.canonical_vocabulary.blank_id(blank) {
                            path += blank_id;
                        } else {
                            eprintln!("No blank in vocabulary");
                        }
                    } else {
                        // step 5.4.4.2
                        // step 5.4.4.2.1
                        recursion_list.insert(related.clone());
                        // step 5.4.4.2.2
                        issuer_copy.issue_identifier(related, vocabulary);
                    }

                    // step 5.4.4.3
                    if !chosen_path.is_empty()
                        && path.len() >= chosen_path.len()
                        && path > chosen_path
                    {
                        continue 'permute;
                    }
                }

                // step 5.4.5
                for related in recursion_list {
                    // step 5.4.5.1
                    let result = self.hash_n_degree_quads(
                        vocabulary,
                        &mut issuer_copy,
                        related.clone(),
                        input_dataset,
                    );
                    // step 5.4.5.2
                    let new_blank = issuer_copy.issue_identifier(related, vocabulary);

                    if let Some(blank_id) = vocabulary.blank_id(&new_blank) {
                        path += blank_id;

                        // step 5.4.5.3
                        path += "<";
                        path += result.0.as_str();
                        path += ">";
                    } else {
                        eprintln!("No blank in vocabulary");
                    }

                    // step 5.4.5.4 is a no-op

                    // step 5.4.5.5
                    if !chosen_path.is_empty()
                        && path.len() >= chosen_path.len()
                        && path > chosen_path
                    {
                        continue 'permute;
                    }
                }

                if chosen_path.is_empty() || path < chosen_path {
                    chosen_path = path;
                    chosen_issuer = issuer_copy;
                }
            }

            // step 5.5
            data_to_hash += &chosen_path;

            // step 5.6
            std::mem::swap(issuer, &mut chosen_issuer);
        }

        // step 6
        self.sha256.update(data_to_hash.as_bytes());
        self.sha256.finalize_hex_and_reset()
    }

    fn hash_related_blank_node(
        &mut self,
        issuer: &Issuer<N::BlankId>,
        vocabulary: &N,
        related: &N::BlankId,
        quad: &NormalizingQuad<N>,
        position: &str,
        input_dataset: &InputDataset<N>,
    ) -> HexHash
    where
        N::BlankId: Clone + Eq + Hash,
        S: Sha256,
    {
        // step 1
        let identifier = if let Some(blank_id) = self.canonical_issuer.get(related) {
            let blank = self
                .canonical_vocabulary
                .blank_id(blank_id)
                .expect("No blank in vocabulary");
            blank.to_string()
        } else if let Some(blank_id) = issuer.get(related) {
            let blank = vocabulary
                .blank_id(blank_id)
                .expect("No blank in vocabulary");
            blank.to_string()
        } else {
            self.hash_first_degree_quads(related.clone(), input_dataset)
                .0
        };

        // step 2
        let mut input = String::from(position);

        // step 3
        if position != "g" {
            input += "<";
            input += quad.predicate().with(&*self.canonical_vocabulary).as_str();
            input += ">";
        }

        // step 4
        input += &identifier;

        // step 5
        self.sha256.update(input.as_bytes());
        self.sha256.finalize_hex_and_reset()
    }

    fn hash_first_degree_quads(
        &mut self,
        identifier: N::BlankId,
        input_dataset: &InputDataset<N>,
    ) -> HexHash
    where
        N::BlankId: Eq + Hash + Clone,
        S: Sha256,
    {
        // Step 1
        let mut nquads = Vec::new();

        // step 2
        if let Some(quad_positions) = self.blank_node_to_quads.get(&identifier).cloned() {
            // step 3
            for quad_position in quad_positions {
                let quad = input_dataset
                    .get(quad_position)
                    .expect("Positions are created from the input dataset");

                // step 3.1, 3.1.1, and 3.1.1.1
                let serizlied = self.serialize_quad(&identifier, quad);

                nquads.push(serizlied);
            }
        }

        // step 4
        nquads.sort();

        // step 5
        let joined = nquads.join("");

        self.sha256.update(joined.as_bytes());
        self.sha256.finalize_hex_and_reset()
    }

    fn serialize_quad(&self, identifier: &N::BlankId, quad: &NormalizingQuad<N>) -> String
    where
        N::BlankId: Clone + Eq,
    {
        let subject = self.serialize_subject(identifier, quad.subject());
        let predicate = quad
            .predicate()
            .with(&*self.canonical_vocabulary)
            .rdf_display()
            .to_string();
        let object = self.serialize_object(identifier, quad.object());
        let graph = quad
            .graph()
            .map(|graph| self.serialize_subject(identifier, graph));

        if let Some(graph) = graph {
            format!("{subject} {predicate} {object} {graph} .\n")
        } else {
            format!("{subject} {predicate} {object} .\n")
        }
    }

    fn serialize_subject(
        &'a self,
        identifier: &N::BlankId,
        subject: &'a QuadSubject<N>,
    ) -> Cow<'a, str>
    where
        N::BlankId: Eq,
    {
        if subject.is_blank() && matches_identifier::<N>(identifier, subject) {
            Cow::Borrowed("_:a")
        } else if subject.is_blank() {
            Cow::Borrowed("_:z")
        } else {
            Cow::Owned(
                subject
                    .with(&*self.canonical_vocabulary)
                    .rdf_display()
                    .to_string(),
            )
        }
    }

    fn serialize_object(&'a self, identifier: &N::BlankId, object: &'a QuadValue<N>) -> Cow<'a, str>
    where
        N::BlankId: Eq,
    {
        match object {
            Term::Iri(iri) => Cow::Owned(display_iri(iri, &*self.canonical_vocabulary)),
            Term::Blank(blank) if blank == identifier => Cow::Borrowed("_:a"),
            Term::Blank(_) => Cow::Borrowed("_:z"),
            Term::Literal(literal) => Cow::Owned(
                literal
                    .with(&*self.canonical_vocabulary)
                    .rdf_display()
                    .to_string(),
            ),
        }
    }
}

fn permute<B>(set: HashSet<B>) -> impl Iterator<Item = Vec<B>>
where
    B: Hash + Eq + Clone,
{
    let len = set.len();

    set.into_iter().permutations(len)
}

pub fn quad_to_string<N>(quad: &NormalizingQuad<N>, vocabulary: &N) -> String
where
    N: Vocabulary,
{
    let subject = quad.subject().with(vocabulary).rdf_display().to_string();
    let predicate = quad.predicate().with(vocabulary).rdf_display().to_string();
    let object = object_to_string(quad.object(), vocabulary);
    let graph = quad
        .graph()
        .map(|graph| graph.with(vocabulary).rdf_display().to_string());

    if let Some(graph) = graph {
        format!("{subject} {predicate} {object} {graph} .\n")
    } else {
        format!("{subject} {predicate} {object} .\n")
    }
}

fn object_to_string<'a, N>(object: &'a QuadValue<N>, vocabulary: &'a N) -> String
where
    N: Vocabulary,
{
    match object {
        Term::Iri(iri) => display_iri(iri, vocabulary),
        Term::Blank(blank) => display_blank(blank, vocabulary),
        Term::Literal(literal) => literal.with(vocabulary).rdf_display().to_string(),
    }
}

fn format_blank<N>(
    blank: &N::BlankId,
    vocabulary: &N,
    formatter: &mut std::fmt::Formatter,
) -> std::fmt::Result
where
    N: Vocabulary,
{
    std::fmt::Display::fmt(
        vocabulary.blank_id(blank).expect("ID exists in vocabulary"),
        formatter,
    )
}

fn display_blank<N>(blank: &N::BlankId, vocabulary: &N) -> String
where
    N: Vocabulary,
{
    vocabulary
        .blank_id(blank)
        .expect("ID exists in vocabulary")
        .to_string()
}

fn format_iri<N>(
    iri: &N::Iri,
    vocabulary: &N,
    formatter: &mut std::fmt::Formatter,
) -> std::fmt::Result
where
    N: Vocabulary,
{
    vocabulary
        .iri(iri)
        .expect("ID exists in vocabulary")
        .rdf_fmt(formatter)
}

fn display_iri<N>(iri: &N::Iri, vocabulary: &N) -> String
where
    N: Vocabulary,
{
    vocabulary
        .iri(iri)
        .expect("ID exists in vocabulary")
        .to_string()
}

fn matches_identifier<N>(identifier: &N::BlankId, subject: &QuadSubject<N>) -> bool
where
    N: Vocabulary,
    N::BlankId: Eq,
{
    match subject {
        Subject::Blank(blank) => blank == identifier,
        Subject::Iri(_) => false,
    }
}

fn subject_as_blank_id<N>(subject: &QuadSubject<N>) -> Option<&N::BlankId>
where
    N: Vocabulary,
{
    match subject {
        Subject::Blank(ref blank) => Some(blank),
        _ => None,
    }
}

fn object_as_blank_id<N>(object: &QuadValue<N>) -> Option<&N::BlankId>
where
    N: Vocabulary,
{
    match object {
        Term::Blank(ref blank) => Some(blank),
        _ => None,
    }
}

impl std::fmt::Display for Security {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Aborted due to time complexity")
    }
}

impl std::error::Error for Security {}
