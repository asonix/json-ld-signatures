use openssl::sha::Sha256;

impl super::Sha256 for Sha256 {
    fn update(&mut self, bytes: &[u8]) {
        openssl::sha::Sha256::update(self, bytes)
    }

    fn finalize_hex_and_reset(&mut self) -> crate::HexHash {
        let this = std::mem::replace(self, Sha256::default());

        crate::HexHash(hex::encode(this.finish()))
    }
}
