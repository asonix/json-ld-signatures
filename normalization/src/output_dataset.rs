use crate::input_dataset::{NormalizingQuad, QuadValue};
use contextual::DisplayWithContext;
use contextual::WithContext;
use rdf_types::{RdfDisplay, Term, Vocabulary};

pub struct OutputDataset<N>
where
    N: Vocabulary,
{
    quads: Vec<NormalizingQuad<N>>,
}

impl<N> OutputDataset<N>
where
    N: Vocabulary,
{
    pub(crate) fn new(mut quads: Vec<NormalizingQuad<N>>, vocabulary: &N) -> Self {
        quads.sort_by_cached_key(|quad| super::quad_to_string(quad, vocabulary));

        Self { quads }
    }

    pub fn quads(&self) -> &[NormalizingQuad<N>] {
        &self.quads
    }

    pub fn into_quads(self) -> Vec<NormalizingQuad<N>> {
        self.quads
    }
}

impl<N> DisplayWithContext<N> for OutputDataset<N>
where
    N: Vocabulary,
{
    fn fmt_with(&self, vocabulary: &N, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for quad in &self.quads {
            quad.subject().with(vocabulary).rdf_fmt(f)?;
            write!(f, " ")?;
            quad.predicate().with(vocabulary).rdf_fmt(f)?;
            write!(f, " ")?;
            write_object(quad.object(), vocabulary, f)?;
            write!(f, " ")?;
            if let Some(graph) = quad.graph() {
                graph.with(vocabulary).rdf_fmt(f)?;
                write!(f, " ")?;
            }

            writeln!(f, ".")?;
        }

        Ok(())
    }
}

impl<N> std::fmt::Debug for OutputDataset<N>
where
    N: Vocabulary,
    N::BlankId: std::fmt::Debug,
    N::Iri: std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("OutputDataset")
            .field("quads", &self.quads)
            .finish()
    }
}

impl<N> Clone for OutputDataset<N>
where
    N: Vocabulary,
    N::BlankId: Clone,
    N::Iri: Clone,
{
    fn clone(&self) -> Self {
        Self {
            quads: self.quads.clone(),
        }
    }
}

fn write_object<'a, N>(
    object: &'a QuadValue<N>,
    vocabulary: &'a N,
    formatter: &mut std::fmt::Formatter,
) -> std::fmt::Result
where
    N: Vocabulary,
{
    match object {
        Term::Literal(ref lit) => lit.with(vocabulary).rdf_fmt(formatter),
        Term::Iri(ref iri) => crate::format_iri(iri, vocabulary, formatter),
        Term::Blank(ref blank) => crate::format_blank(blank, vocabulary, formatter),
    }
}
