use json_ld::{RdfQuads, ValidId as Subject};
use locspan::Location;
use rdf_types::{
    generator::{Blank, WithMetadata},
    BlankIdVocabulary, IriVocabulary, Quad, Vocabulary, VocabularyMut,
};
use std::hash::Hash;

pub(crate) type QuadSubject<N> =
    Subject<<N as IriVocabulary>::Iri, <N as BlankIdVocabulary>::BlankId>;
pub(crate) type QuadValue<N> =
    json_ld::rdf::Value<<N as IriVocabulary>::Iri, <N as BlankIdVocabulary>::BlankId>;

pub(crate) type NormalizingQuad<N> =
    Quad<QuadSubject<N>, QuadSubject<N>, QuadValue<N>, QuadSubject<N>>;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Position(usize);

pub(crate) struct InputDataset<N>
where
    N: Vocabulary,
{
    quads: Vec<NormalizingQuad<N>>,
}

type BlankNodeGenerator<D> = WithMetadata<Blank, Location<D>>;

impl<N> InputDataset<N>
where
    N: Vocabulary,
{
    pub(crate) fn from_rdf_quads<R, D>(
        rdf_quads: R,
        vocabulary: &mut N,
        generator: &mut BlankNodeGenerator<D>,
    ) -> InputDataset<N>
    where
        R: RdfQuads<N::Iri, N::BlankId, Location<D>>,
        D: Clone,
        N: VocabularyMut,
        N::Iri: Clone + Eq + Hash + Send + Sync,
        N::BlankId: Clone + Eq + Hash + Send + Sync,
    {
        let quads = rdf_quads
            .rdf_quads_with(vocabulary, generator, None)
            .map(|quad| {
                let (subject, predicate, object, graph) = quad.into_parts();

                Quad(
                    subject.into_owned(),
                    predicate.into_owned(),
                    object,
                    graph.cloned(),
                )
            })
            .collect();

        InputDataset { quads }
    }

    pub(crate) fn get(&self, position: Position) -> Option<&NormalizingQuad<N>> {
        self.quads.get(position.0)
    }

    pub(crate) fn quads(&self) -> impl Iterator<Item = (Position, &'_ NormalizingQuad<N>)> + '_ {
        self.quads
            .iter()
            .enumerate()
            .map(|(index, quad)| (Position(index), quad))
    }
}

impl<N> std::fmt::Debug for InputDataset<N>
where
    N: Vocabulary,
    N::BlankId: std::fmt::Debug,
    N::Iri: std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("InputDataset")
            .field("quads", &self.quads)
            .finish()
    }
}
