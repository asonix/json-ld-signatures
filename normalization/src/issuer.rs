use indexmap::IndexMap;
use json_ld::ValidId as Subject;
use rdf_types::{generator::Blank, BlankIdVocabularyMut, Vocabulary};
use std::hash::Hash;

pub(crate) struct Issuer<B> {
    // Identifier Prefix and Identifier Counter
    blank_node_generator: Blank,
    // Issued Identifier List
    issued_identifier_list: IndexMap<B, B>,
}

impl<B> Issuer<B> {
    pub(crate) fn new() -> Self {
        Self {
            blank_node_generator: canonicalization_node_generator(),
            issued_identifier_list: Default::default(),
        }
    }

    pub(crate) fn new_with_prefix(prefix: &str) -> Self {
        Self {
            blank_node_generator: make_issuer(prefix),
            issued_identifier_list: Default::default(),
        }
    }

    pub(crate) fn iter(&self) -> impl Iterator<Item = (&B, &B)> {
        self.issued_identifier_list.iter()
    }

    pub(crate) fn get(&self, identifier: &B) -> Option<&B>
    where
        B: Eq + Hash,
    {
        self.issued_identifier_list.get(identifier)
    }

    pub(crate) fn contains(&self, identifier: &B) -> bool
    where
        B: Eq + Hash,
    {
        self.issued_identifier_list.contains_key(identifier)
    }

    pub(crate) fn issue_identifier<N>(&mut self, identifier: B, vocabulary: &mut N) -> N::BlankId
    where
        N: Vocabulary<BlankId = B> + BlankIdVocabularyMut,
        B: Eq + Hash + Clone,
    {
        use rdf_types::Generator;

        // step 1
        if let Some(blank) = self.get(&identifier) {
            return blank.clone();
        }

        // step 2 and 4
        let blank = match self.blank_node_generator.next(vocabulary) {
            Subject::Blank(blank) => blank,
            Subject::Iri(_) => unreachable!("Blank ID generators should only generate blank IDs"),
        };

        // step 3
        self.issued_identifier_list
            .insert(identifier, blank.clone());

        // step 5
        blank
    }
}

fn canonicalization_node_generator() -> Blank {
    make_issuer("c14n")
}

fn make_issuer(prefix: &str) -> Blank {
    Blank::new_with_prefix(String::from(prefix))
}

impl<B> Clone for Issuer<B>
where
    B: Clone,
{
    fn clone(&self) -> Self {
        Self {
            blank_node_generator: Blank::new_full(
                self.blank_node_generator.prefix().to_string(),
                self.blank_node_generator.count(),
            ),
            issued_identifier_list: self.issued_identifier_list.clone(),
        }
    }
}

impl<B> Default for Issuer<B> {
    fn default() -> Self {
        Self::new()
    }
}
