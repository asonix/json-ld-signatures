use sha2::{Digest, Sha256};

impl super::Sha256 for Sha256 {
    fn update(&mut self, bytes: &[u8]) {
        Digest::update(self, bytes)
    }

    fn finalize_hex_and_reset(&mut self) -> crate::HexHash {
        let output = self.finalize_reset();

        crate::HexHash(hex::encode(output))
    }
}
