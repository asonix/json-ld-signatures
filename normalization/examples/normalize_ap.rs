use contextual::WithContext;
use iref::Iri;
use json_ld::{
    syntax::{Parse, Value},
    JsonLdProcessor, RemoteDocument, ReqwestLoader,
};
use locspan::{Location, Span};
use rdf_types::{generator::Blank, IriVocabularyMut};
use reqwest::Client;
use static_iref::iri;

type AnyError = Box<dyn std::error::Error + Send + Sync>;

#[tokio::main]
async fn main() -> Result<(), AnyError> {
    let client = Client::builder().user_agent("json-ld-playground").build()?;

    let iris = [
        iri!("https://relay.asonix.dog/actor"),
        iri!("https://masto.asonix.dog/actor"),
        iri!("https://masto.asonix.dog/users/asonix"),
        iri!("https://masto.asonix.dog/users/kumu"),
        iri!("https://yiff.life/users/6my"),
        iri!("https://meow.social/users/6my"),
    ];

    for iri in iris {
        let document = client
            .get(iri.as_str())
            .header("accept", "application/activity+json")
            .send()
            .await?
            .text()
            .await?;

        normalize_document(iri, &document).await?;
    }

    Ok(())
}

async fn normalize_document(iri: Iri<'static>, document: &str) -> Result<(), AnyError> {
    let mut vocabulary: rdf_types::IndexVocabulary = rdf_types::IndexVocabulary::new();

    let iri_index = vocabulary.insert(iri);

    let input = RemoteDocument::new(
        Some(iri_index.clone()),
        Some("application/activity+json".parse()?),
        Value::parse_str(document, |span| Location::new(iri_index.clone(), span))?,
    );

    let mut loader = ReqwestLoader::default();

    let mut expanded = input.expand_with(&mut vocabulary, &mut loader).await?;

    let mut pre_gen = Blank::new().with_metadata(Location::new(iri_index.clone(), Span::default()));

    expanded.identify_all_with(&mut vocabulary, &mut pre_gen);

    let output_document = json_ld_normalization::normalize::<_, _, _, sha2::Sha256>(
        &mut vocabulary,
        iri_index,
        expanded.0,
        true,
    )?;

    println!("{}", output_document.with(&vocabulary));

    Ok(())
}
